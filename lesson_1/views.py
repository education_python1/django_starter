from django.shortcuts import render


def index(request):
    return render(request, 'lesson_1/index.html')


def home(request):
    return render(request, 'lesson_1/home.html')


def reverse(request):
    original_text = request.GET['user_text']
    number_of_words = len(original_text.split())
    reversed_text = original_text[::-1]
    return render(request, 'lesson_1/reverse.html', context={"original_text": original_text,
                                                             "number_of_words": number_of_words,
                                                             "reversed_text": reversed_text})
