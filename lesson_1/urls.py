from django.urls import path, re_path
from . import views

urlpatterns = [
    re_path('^home/$', views.home, name='home'),
    re_path('^reverse/$', views.reverse, name='reverse'),
    re_path('^', views.index, name='index'),
]
