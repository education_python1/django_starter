from django.shortcuts import render

# from django.views.generic import View
# from django.http import HttpResponse


def index(request):
    return render(request, 'lesson_11/index.html')


# class MyView(View):
#
#     def get(self, request):
#         return HttpResponse("Hello!")

