from requests import get
from os import environ


def get_weather(city):
    key = environ.get("WEATHER_API_KEY")
    weather_api = f'https://api.openweathermap.org/data/2.5/weather?q={city}&APPID={key}'
    return get(weather_api)

