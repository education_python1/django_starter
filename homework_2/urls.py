from django.urls import path, re_path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('home/', views.home, name='home-view'),
    path('book/', views.book_warn, name='book-warn'),
    path('book/<str:chapter>/', views.book, name='book'),
    path('index/', views.index, name='index-view'),
    path('bio/', views.bio_warn, name='bio-warn'),
    path('bio/<username>/', views.bio, name='bio'),
    re_path('^weather/$', views.weather, name='weather'),
]
