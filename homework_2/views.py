from django.shortcuts import render
from django.http import HttpResponse
from requests import get
from os import environ
from . import utils


# from django.views.generic import View


def index(request):
    return render(request, 'homework_2/index.html')


def home(request):
    return render(request, 'homework_2/home.html')


def book(request, chapter):
    return render(request, 'homework_2/book.html', context={"chapter": chapter})


def book_warn(request):
    return HttpResponse("Додайте до URL назву розділу")


def bio(request, username):
    return render(request, 'homework_2/bio.html', context={"username": username})


def bio_warn(request):
    return HttpResponse("Додайте до URL ім'я користувача")


def weather(request):

    city = request.GET['city']
    response = utils.get_weather(city)

    if response.status_code != 200:
        return HttpResponse(f'<script>alert("City {city} does not exist!");</script>')

    dict_response = response.json()

    return render(request, 'homework_2/weather.html', context={"country": dict_response['sys']['country'],
                                                               "city": city,
                                                               "longitude": dict_response['coord']['lon'],
                                                               "latitude": dict_response['coord']['lat'],
                                                               "weather": dict_response['weather'][0]['description'],
                                                               "temperature": dict_response['main']['temp']
                                                               })
