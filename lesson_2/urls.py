from django.urls import re_path
from lesson_2 import views

urlpatterns = [
    re_path('^', views.index, name='index'),
]
